### classes[0] = "Presentation"

#### Perspetivas em Bioinformática 2021-2022

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

## Informação prática

* &shy;<!-- .element: class="fragment" --> Horário: 2ª-Feira - 11:00 às 13:00 **Sala S1.10**
* &shy;<!-- .element: class="fragment" --> Perguntas? f.pina.martins©estbarreiro.ips.pt
* &shy;<!-- .element: class="fragment" --> [Moodle](https://moodle.ips.pt/2122/course/view.php?id=1720)
* &shy;<!-- .element: class="fragment" --> Office hours: TBD

---

## Avaliação


* &shy;<!-- .element: class="fragment" --><font color="orange">Contínua</font> **OU** <font color="deeppink">exame 1ª e/ou 2ª época</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">Avaliação contiunua</font>
    * &shy;<!-- .element: class="fragment" --><font color="orange">20% - Contexto sala de aula + "Homework"</font>
    * &shy;<!-- .element: class="fragment" --><font color="orange">30% - 1º trabalho</font>
    * &shy;<!-- .element: class="fragment" --><font color="orange">30% - 2º trabalho</font>
    * &shy;<!-- .element: class="fragment" --><font color="orange">20% - Teste</font>
    * &shy;<!-- .element: class="fragment" --><font color="orange">Sem notas mínimas</font>
* &shy;<!-- .element: class="fragment" -->Realizar 3/4 das componentes da <font color="orange">avaliação contínua</font> impede acesso a <font color="deeppink">exame de 1ª época</font>
  * &shy;<!-- .element: class="fragment" --><font color="deeppink">Avaliação por exame</font>
    * &shy;<!-- .element: class="fragment" --><font color="deeppink">100% - exame</font>

---

## What is "bioinformatics"?

* &shy;<!-- .element: class="fragment" -->There really is no consensus on the subject
* &shy;<!-- .element: class="fragment" -->Loose definitions

|||

## Such as?


* &shy;<!-- .element: class="fragment" -->Develop methodologies and analyses tools to <font color="red">-----------</font> biological data
  * &shy;<!-- .element: class="fragment" --><font color="orange">explore</font>
  * &shy;<!-- .element: class="fragment" --><font color="green">store</font>
  * &shy;<!-- .element: class="fragment" --><font color="cyan">organize</font>
  * &shy;<!-- .element: class="fragment" --><font color="purple">systematize</font>
  * &shy;<!-- .element: class="fragment" --><font color="yellow">annotate</font>
  * &shy;<!-- .element: class="fragment" --><font color="navy">visualize</font>
  * &shy;<!-- .element: class="fragment" --><font color="grey">query</font>
  * &shy;<!-- .element: class="fragment" --><font color="deeppink">mine</font>
  * &shy;<!-- .element: class="fragment" --><font color="#009933">understand</font>
  * &shy;<!-- .element: class="fragment" --><font color="#FFCC66">interpret</font>

|||

## How do we get there?

* &shy;<!-- .element: class="fragment" -->Biology
* &shy;<!-- .element: class="fragment" -->Statistics
* &shy;<!-- .element: class="fragment" -->Mathematics
* &shy;<!-- .element: class="fragment" -->Computer Science

</br>
</br>

* &shy;<!-- .element: class="fragment" -->[Searls 2010](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000809) - The roots of Bioinformatics
* &shy;<!-- .element: class="fragment" -->[Abdurakhmonov 2016](https://www.intechopen.com/books/bioinformatics-updated-features-and-applications/bioinformatics-basics-development-and-future) - Bioinformatics' development
* &shy;<!-- .element: class="fragment" -->[Bourne 2021](https://doi.org/10.1371/journal.pbio.3001165) - Is Bioinformatics dead?

---

## Overview

* &shy;<!-- .element: class="fragment" -->DNA and proteins
* &shy;<!-- .element: class="fragment" -->Virtual Machines
* &shy;<!-- .element: class="fragment" -->GNU/Linux command line
* &shy;<!-- .element: class="fragment" -->Text Processing Software and reference management
* &shy;<!-- .element: class="fragment" -->Digital DNA sequences
* &shy;<!-- .element: class="fragment" -->Short intro to programming
* &shy;<!-- .element: class="fragment" -->Alternate perspectives seminars

---

## My own background

* &shy;<!-- .element: class="fragment" -->Graduation in Marine Biology (2004)

&shy;<!-- .element: class="fragment" -->![dolphin](presentation_assets/dolphin.jpg)

* &shy;<!-- .element: class="fragment" -->MSc in "Evolutionary and Developmental Biology" (2006)
* &shy;<!-- .element: class="fragment" -->PhD in "Global Change Biology and Ecology" (2018)

|||

## Wait, what?

* &shy;<!-- .element: class="fragment" data-fragment-index="1" -->First contact during MSc
  * &shy;<!-- .element: class="fragment" data-fragment-index="2" -->PERL
  * &shy;<!-- .element: class="fragment" data-fragment-index="2" -->Sequence data
  * &shy;<!-- .element: class="fragment" data-fragment-index="2" -->GNU/Linux
* &shy;<!-- .element: class="fragment" data-fragment-index="3" -->Research fellowships
  * &shy;<!-- .element: class="fragment" data-fragment-index="4" -->Python
  * &shy;<!-- .element: class="fragment" data-fragment-index="4" -->Sysadmin
  * &shy;<!-- .element: class="fragment" data-fragment-index="4" -->Software management
* &shy;<!-- .element: class="fragment" data-fragment-index="5" -->PhD
  * &shy;<!-- .element: class="fragment" data-fragment-index="6" -->Reproducibility
  * &shy;<!-- .element: class="fragment" data-fragment-index="6" -->Automation
  * &shy;<!-- .element: class="fragment" data-fragment-index="6" -->Performance

---

## What about you?

---

## References

* [Searls 2010](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000809)
* [Abdurakhmonov 2016](https://www.intechopen.com/books/bioinformatics-updated-features-and-applications/bioinformatics-basics-development-and-future)
* [Bourne 2021](https://doi.org/10.1371/journal.pbio.3001165)
