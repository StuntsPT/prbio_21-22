# Assignment 02


### Deadline:

January 27th 2022


### Format:

Text files


### Delivery:

`git`


## Introduction

You have now learned to use a version control system (VCS) - `git`. Your second task is based on it.
This is a very practical task, and the work-flow you will have to master for it will likely be useful during the rest of your career in bioinformatics.
A decent grip on \*NIX shell is a big help for this task.

The words "firstname" and "lastname" represent your given name and family name respectively for the rest of this assignment.


## Objectives

1. *Hand-made* sequences
    * Rename the file you submitted as your first homework (from **classes[1]**) as `firstname_lastname_handmade_sequences.fasta`
    * If you feel you can improve this homework, now is you last chance to correct it
        * In case you make any changes to your original sequences, please rename the file `firstname_lastname_handmade_sequences_corrected.fasta` instead

2. Reference mapping data
    * Name the files you created for your second homework (from **classes[5]**) as `firstname_lastname_fstab.txt` and `firstname_lastname_screenfetch.txt` (the fstab and screenfetch files respectively)

3. DNA alignments
    * Download between 20 and 50 sequences of a species (or genera) of your choice, but make sure all sequences are from the same gene fragment, from a DNA sequence database
    * Align your DNA sequences using an alignment software of your choice
    * Name your "original" and "aligned" files `firstname_lastname_sequences.fasta` and `firstname_lastname_aligned.fasta` respectively

4. `gitting` it out
    * Consider the following `git` [repository](https://github.com/StuntsPT/pr_bio_2021-2022_assignments)
    * File a new issue on the repository, where you will state that your username is missing from the `students.csv` file.
    * Add the missing data to `students.csv`
        * "Number" represents your student number
        * "Username" represents your github username
        * Do not make any other changes to the file
    * Commit these changes with an appropriate message
    * Add the files mentioned in steps 1,2 and 3 to the repository in your account
    * Get your changes accepted into the original repository
        * Due to a recent change in github's password use policy, it is highly recommended that you clone your repository using the `ssh` link rather than the `https` one.
        * This means you will need to generate an *ssh key*. [Here is a video explaining](https://www.youtube.com/watch?v=5Fcf-8LPvws) how to do it **UNTIL 2:20** (the rest does not apply to your use case).
        * Once you have generated your ssh key pair, you need to add your public key to github. [Here is how](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account).


## Methodology

1. *Hand-made* sequences
    * Methodology is described [here](https://stuntspt.gitlab.io/prbio_21-22/classes/class_02/index.html#/33)

2. Reference mapping data
    * Methodology is described [here](https://stuntspt.gitlab.io/prbio_21-22/classes/class_06/index.html#/19)

3. DNA alignments
    * Methodology for this task is detailed [here](https://stuntspt.gitlab.io/prbio_21-22/classes/class_07/index.html#/7) and [here](https://stuntspt.gitlab.io/prbio_21-22/classes/class_07/index.html#/10/1)

4. `gitting` it out
    * Use github's interface to open a new issue in the repository
    * Fork [the repository](https://github.com/StuntsPT/pr_bio_2021-2022_assignments) under your own account
    * Clone this repository to your local machine
    * Add your username to the line with your student number to the file `students.csv`. **Separate the name and number with a ",", just like in the example**
    * Commit this change to your repository with an appropriate message
  
    * Consider the directory `handmade_seqs`
        * Add **one** of the following files to this directory:
            * `firstname_lastname_handmade_sequences.fasta`
            * `firstname_lastname_handmade_sequences_corrected.fasta`
    * Commit this change to your repository with an appropriate message

    * Consider the directory `unique_os_data`
        * Add two files to this directory:
            * `firstname_lastname_fstab.txt`
            * `firstname_lastname_screenfetch.txt`
    * Commit this change to your repository with an appropriate message

    * Consider the directory `dna_alignments`
        * Add two files to this directory:
            * `firstname_lastname_sequences.fasta`
            * `firstname_lastname_aligned.fasta`
    * Commit this change to your repository with an appropriate message

    * Details on this task are available [here](https://stuntspt.gitlab.io/prbio_21-22/classes/class_08/index.html#/5) from the linked slide to the end of the presentation

    * Once all this is done, open a "pull request" so that your changes can be included under the main repository
