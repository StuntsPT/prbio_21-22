# Assignment 01

### Deadline:

December 6th 2021


### Format:

Work in groups of 4


### Delivery:

PDF **and** *docx*/*odt*/*tex* via E-mail


## Introduction

During the course's first weeks you took a *sneak peek* at the world of bioinformatics.
Now you will have to do some **research** to complete your new assignment.


## Objectives

* Write a short essay (1250-3000 words) on a bioinformatics topic of your choice
  * Describe the biological problem;
  * Pinpoint how it is related to bioinformatics;
  * Present some of the used software;
  * Find at least three research papers on the topic;
    * Report the respective findings in short paragraphs;


## Rules

* Word count is not mandatory, it's more of a "ballpark" - If you are below the lower limit you *probably* don't have enough material, whereas if you go above the limit you *probably* have too much "filler"
* You can use any text processing software to write your report. But it has to be sent as both a finished document (PDF or HTML), **and** as "source" (odt, docx, or tex)
* Use *Styles*, as your grade literally depends on it
* Create a table of contents
* Have *at least* one (captioned and referenced) figure or table
* References have to be both in-text **and** on a reference list
* Use reference management software
  * The style you should use is "American Psychological Association (APA) 7th edition"
* The report can either be written in English or Portuguese. It's your choice
  * But realistically, you should start writing in English
* Don't forget to include a cover page
* Make sure you proofread your work. You only get one shot.


## Scientific literature search resources:

* [Google scholar](https://scholar.google.com/)
* [b-On](https://www.b-on.pt/)
* [PubMed](https://pubmed.ncbi.nlm.nih.gov/)
